# Example GraphQL Server (Apollo)
Пример работы GraphQL с использованием библиотеки Apollo.

## Установка и запуск проекта
##### Установка Docker и Docker-compose
Мануал установки докера описан под систему Ubuntu >= 18.04.  
Для установки необходимо выполнить все команды ниже:
```shell script
sudo apt-get update
sudo apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg-agent \
     software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
     $(lsb_release -cs) \
     stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker $USER
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```
После успешного выполнения всех команд необходимо перезапустить систему:
```shell script
reboot
```
##### Запуск проекта
>Все команды следует выполнять в корне проекта

0. Необходимо задать файл переменного окружения, для примера есть файл `.env.example`, в котором описан пример необходимых переменных. 

    Скопируйте содержимое файла `.env.example` в файл `.env`:
    
    ```shell script
    cp .env.example .env
    ```
   >Единственно, что нужно сделать — это указать значение в переменной `DB_PASS`. Это пароль для базы данных, создайте его сложным (генератор паролей: https://randstuff.ru/password/).

0. Для запуска сервера выполните команду:
   ```shell script
   docker-compose up -d
   ```
   >Она нам создаст виртуальный сервер на базе Ubuntu и также поднимет базу данных.  
   После того, как команда завершит свою работу — необходимо выполнить следущие команды.

0. Дальше необходимо выполнить миграции базы данных и заполнить их тестовыми данными, для этого выполняем команды ниже:

   Запускаем миграции:
   ```shell script
   docker-compose exec app npx knex migrate:latest
   ```
   >У нас должны создаться таблицы в базе данных
   
   Заполняем таблицы тестовыми данными (можно пропустить, если не нужны):
   ```shell script
   docker-compose exec app npx knex seed:run
   ```
0. Открываем наше приложение по адресу http://localhost:4000 и перед нами появится редактор запросов для GraphQL.
   
   Для теста в нём напишем запрос для получения всех книг и их авторов:
   ```graphql
   query {
     allBooks {
       name
       authors {
         lastName
         firstName
       }
     }
   }
   ```
   Чтобы выполнить запрос нажмите сочетание клавиш `Ctrl` + `Enter`.  
   Справа должны появиться данные в формате `JSON`.

---

Другие команды:

Остановка приложения:
```shell script
docker-compose stop
```
Перезапуск приложения:
```shell script
docker-compose restart
```
Откат миграций:
>Если необходимо быстро избавиться от данных, то можно откатить миграции и снова накатить
```shell script
docker-compose exec app npx knex migrate:rollback
```

Данные для подключения к базе данных извне:
> Все эти данные находятся в файле `.env`.
* Хост: localhost
* Порт: 55432
* Пользователь: postgreadmin
* Название базы данных: postgre
* Пароль: `тот, который вы установили`
