const faker = require('faker')
const { chunk } = require('lodash')
const config = require('../../config')
const { getAuthors } = require('./02-authors')
const { getBooks } = require('./03-books')

const getRandomAuthorIds = (authors, maxBookAuthors = 3) => {
  const quantityAuthors = authors.length
  const realyMaxBookAuthors = quantityAuthors >= maxBookAuthors ? maxBookAuthors : quantityAuthors
  const randomQuantityBookAuthors = faker.random.number(realyMaxBookAuthors)
  const bookAuthorIds = []
  while (bookAuthorIds.length < randomQuantityBookAuthors) {
    const author = faker.random.arrayElement(authors)
    if (!bookAuthorIds.includes(author.id)) {
      bookAuthorIds.push(author.id)
    }
  }
  return bookAuthorIds
}

const authorToBook = (authors, books) => {
  return books.flatMap(
    book => getRandomAuthorIds(authors).map(authorId => ({
      author_id: authorId,
      book_id: book.id
    }))
  )
}

exports.getRandomAuthorIds = getRandomAuthorIds
exports.authorToBook = authorToBook

exports.seed = async knex => {
  if (!config.isProduction) await knex('author_book').del()
  const authors = getAuthors()
  const books = getBooks()
  return Promise.all(
    chunk(authorToBook(authors, books)).map(batch => {
      return knex('author_book').insert(batch)
    })
  )
}
