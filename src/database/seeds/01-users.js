const bcrypt = require('bcrypt')
const config = require('../../config')

const users = [
  {
    id: '7fcc6415-30c4-4b94-a0ab-9b24e58b0fa2',
    first_name: 'Ryan',
    last_name: 'Dahl'
  },
  {
    id: 'e8edb86a-37e8-4f80-b500-d74c40fecfac',
    first_name: 'Adrian',
    last_name: 'Lamo'
  },
  {
    id: '8d9dd7f8-b389-4c74-848b-65592bc96e3c',
    first_name: 'Dan',
    last_name: 'Abramov'
  }
]

const getUsers = () => {
  return users.map(user => ({
    ...user,
    password: bcrypt.hashSync('Z0987654321', 10)
  }))
}

exports.getUsers = getUsers

exports.seed = async knex => {
  if (!config.isProduction) await knex('users').del()
  return knex('users').insert(getUsers())
}
