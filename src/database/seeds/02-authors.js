const config = require('../../config')

const authors = [
  {
    id: 'c279982b-56f0-4592-ba46-460fe85caae8',
    first_name: 'Tyrion',
    last_name: 'Lannister'
  },
  {
    id: 'bf849cf1-8b1c-4237-ac57-7ea7427e948b',
    first_name: 'Cersei',
    last_name: 'Lannister'
  },
  {
    id: '05fe4a00-c640-4715-9c2e-ed0e3cda592b',
    first_name: 'Daenerys',
    last_name: 'Targaryen'
  },
  {
    id: 'd2c24691-684b-4f58-9d05-93d9d179da4b',
    first_name: 'Jon',
    last_name: 'Snow'
  },
  {
    id: '553e64af-c75e-4200-950c-6a3a2c8c7907',
    first_name: 'Sansa',
    last_name: 'Stark'
  },
  {
    id: 'c317dbd5-9d46-4678-96c2-e53ae8ec6464',
    first_name: 'Jorah',
    last_name: 'Mormont'
  }
]

const getAuthors = () => {
  return authors
}

exports.getAuthors = getAuthors

exports.seed = async knex => {
  if (!config.isProduction) await knex('authors').del()
  return knex('authors').insert(getAuthors())
}
