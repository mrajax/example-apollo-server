const faker = require('faker')
const config = require('../../config')

const books = [
  {
    id: '89286b4a-b84a-4800-af6b-6d219c2c2564',
    name: 'Game of thrones'
  },
  {
    id: '7b8f803d-bf47-40c7-86e8-295906de700b',
    name: 'Battle of kings'
  },
  {
    id: '97d8c828-1fa2-420a-83af-b7bdf090244e',
    name: 'Storm of swords'
  }
]

const getBooks = () => {
  return books.map(book => {
    return {
      ...book,
      description: faker.lorem.sentences(20)
    }
  })
}

exports.getBooks = getBooks

exports.seed = async knex => {
  if (!config.isProduction) await knex('books').del()
  return knex('books').insert(getBooks())
}
