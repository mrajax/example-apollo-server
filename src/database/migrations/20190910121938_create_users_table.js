exports.up = async knex => {
  return knex.schema.createTable('users', table => {
    table
      .uuid('id')
      .unique()
      .primary()
      .notNullable()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .string('first_name')
      .notNullable()
    table
      .string('last_name')
      .notNullable()
    table
      .string('password')
      .notNullable()
    table.timestamps(true, true)
  })
}

exports.down = knex => {
  return knex.schema.dropTableIfExists('users')
}
