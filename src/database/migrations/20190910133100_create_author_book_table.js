exports.up = knex => {
  return knex.schema.createTable('author_book', table => {
    table
      .uuid('author_id')
      .notNullable()
      .references('authors.id')
      .onDelete('cascade')
    table
      .uuid('book_id')
      .notNullable()
      .references('books.id')
      .onDelete('cascade')
    table.unique(['author_id', 'book_id'])
  })
}

exports.down = knex => {
  return knex.schema.dropTableIfExists('author_book')
}
