exports.up = knex => {
  return knex.schema.createTable('comments', table => {
    table
      .uuid('id')
      .unique()
      .primary()
      .notNullable()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .text('body')
      .notNullable()
    table
      .uuid('user_id')
      .nullable()
      .references('users.id')
      .onDelete('set null')
    table
      .uuid('book_id')
      .notNullable()
      .references('books.id')
      .onDelete('cascade')
    table.timestamps(true, true)
  })
}

exports.down = knex => {
  return knex.schema.dropTableIfExists('comments')
}
