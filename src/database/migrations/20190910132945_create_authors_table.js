exports.up = knex => {
  return knex.schema.createTable('authors', table => {
    table
      .uuid('id')
      .unique()
      .primary()
      .notNullable()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .string('first_name')
      .notNullable()
    table
      .string('last_name')
      .notNullable()
  })
}

exports.down = knex => {
  return knex.schema.dropTableIfExists('authors')
}
