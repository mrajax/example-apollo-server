exports.up = knex => {
  return knex.schema.createTable('books', table => {
    table
      .uuid('id')
      .unique()
      .primary()
      .notNullable()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .string('name')
      .notNullable()
    table
      .text('description')
      .nullable()
  })
}

exports.down = knex => {
  return knex.schema.dropTableIfExists('books')
}
