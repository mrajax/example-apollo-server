const { Model } = require('objection')

class User extends Model {
  static get tableName() {
    return 'users'
  }

  static relationMappings() {
    const Comment = require('./comment')

    return {
      comments: {
        relation: Model.HasManyRelation,
        modelClass: Comment,
        join: {
          from: 'users.id',
          to: 'comments.user_id'
        }
      }
    }
  }
}

module.exports = User
