const { Model } = require('objection')

class Author extends Model {
  static get tableName() {
    return 'authors'
  }

  static relationMappings() {
    const Book = require('./book')

    return {
      books: {
        relation: Model.ManyToManyRelation,
        modelClass: Book,
        join: {
          from: 'authors.id',
          through: {
            from: 'author_book.author_id',
            to: 'author_book.book_id'
          },
          to: 'books.id'
        }
      }
    }
  }
}

module.exports = Author
