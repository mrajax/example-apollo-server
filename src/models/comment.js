const { Model } = require('objection')

class Comment extends Model {
  static get tableName() {
    return 'comments'
  }

  static relationMappings() {
    const User = require('./user')
    const Book = require('./book')

    return {
      author: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'comments.user_id',
          to: 'users.id'
        }
      },
      book: {
        relation: Model.BelongsToOneRelation,
        modelClass: Book,
        join: {
          from: 'comments.book_id',
          to: 'books.id'
        }
      }
    }
  }
}

module.exports = Comment
