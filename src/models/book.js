const { Model } = require('objection')

class Book extends Model {
  static get tableName() {
    return 'books'
  }

  static relationMappings() {
    const Author = require('./author')
    const Comment = require('./comment')

    return {
      authors: {
        relation: Model.ManyToManyRelation,
        modelClass: Author,
        join: {
          from: 'books.id',
          through: {
            from: 'author_book.book_id',
            to: 'author_book.author_id'
          },
          to: 'authors.id'
        }
      },
      comments: {
        relation: Model.HasManyRelation,
        modelClass: Comment,
        join: {
          from: 'books.id',
          to: 'comments.book_id'
        }
      }
    }
  }
}

module.exports = Book
