const fs = require('fs')
const path = require('path')
const Knex = require('knex')
const { ApolloServer, PubSub } = require('apollo-server')
const { Model } = require('objection')
const resolvers = require('./resolvers')
const { isDevelopment } = require('./config')
const knexConfig = require('../knexfile')

const schemaPath = path.join(__dirname, 'schema.graphql')
const typeDefs = fs.readFileSync(schemaPath, 'utf8')

const knex = Knex(knexConfig)
const pubsub = new PubSub()

Model.knex(knex)

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: { knex, pubsub },
  cors: false,
  debug: isDevelopment,
  introspection: isDevelopment,
  playground: isDevelopment
})

server.listen().then(({ port }) => {
  console.log(`🚀🚀Server ready on internal port ${port}`)
})
