require('dotenv').config()

const {
  APP_TIMEZONE = 'Europe/Moscow',
  NODE_ENV = 'development',
  DB_HOST = 'db',
  DB_USER = 'postgreadmin',
  DB_PASSWORD = null,
  DB_DATABASE = 'postgre'
} = process.env

const isProduction = NODE_ENV === 'production'
const isDevelopment = !isProduction

module.exports = Object.freeze({
  environment: NODE_ENV,
  timezone: APP_TIMEZONE,
  database: {
    host: DB_HOST,
    user: DB_USER,
    password: DB_PASSWORD,
    database: DB_DATABASE
  },
  isProduction,
  isDevelopment
})
