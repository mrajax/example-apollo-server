const subscribe = (parent, args, context) => {
  return context.pubsub.asyncIterator(['COMMENT_ADDED'])
}

const resolve = payload => payload

module.exports = { subscribe, resolve }
