const allBooks = require('./queries/all-books')
const allAuthors = require('./queries/all-authors')
const allUsers = require('./queries/all-users')
const createAuthor = require('./mutations/create-author')
const createBook = require('./mutations/create-book')
const addAuthorsToBook = require('./mutations/add-authors-to-book')
const addCommentBook = require('./mutations/add-comment-book')
const commentAdded = require('./subscriptions/comment-added')
const getBookById = require('./queries/get-book-by-id')
const deleteBook = require('./mutations/delete-book')

module.exports = {
  Query: {
    allBooks,
    allAuthors,
    allUsers,
    getBookById
  },
  Mutation: {
    createAuthor,
    createBook,
    addAuthorsToBook,
    addCommentBook,
    deleteBook
  },
  Subscription: {
    commentAdded
  }
}
