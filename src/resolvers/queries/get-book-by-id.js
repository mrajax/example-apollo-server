const { UserInputError } = require('apollo-server')
const Book = require('../../models/book')

module.exports = async (parent, args) => {
  let book

  await Book.query()
    .eager('[authors, comments]')
    .modifyEager('authors', builder => {
      builder.select([
        'id',
        'first_name as firstName',
        'last_name as lastName'
      ])
    })
    .modifyEager('comments', builder => {
      builder.select([
        'id',
        'body',
        'user_id as userId',
        'book_id as bookId',
        'created_at as createdAt',
        'updated_at as updatedAt',
      ])
    })
    .findById(args.id)
    .then(b => book = b)
    .catch(() => {})

  if (!book) throw new UserInputError('Book not found')

  return book
}
