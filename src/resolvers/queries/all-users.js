const User = require('../../models/user')

module.exports = () => {
  return User.query()
    .select([
      'id',
      'first_name as firstName',
      'last_name as lastName',
      'created_at as createdAt',
      'updated_at as updatedAt'
    ])
    .eager('comments')
    .modifyEager('comments', builder => {
      builder.select([
        'id',
        'body',
        'user_id as userId',
        'book_id as bookId',
        'created_at as createdAt',
        'updated_at as updatedAt',
      ])
    })
}
