const Book = require('../../models/book')

module.exports = () => {
  return Book.query()
    .eager('[authors, comments]')
    .modifyEager('authors', builder => {
      builder.select([
        'id',
        'first_name as firstName',
        'last_name as lastName'
      ])
    })
    .modifyEager('comments', builder => {
      builder.select([
        'id',
        'body',
        'user_id as userId',
        'book_id as bookId',
        'created_at as createdAt',
        'updated_at as updatedAt',
      ])
    })
}
