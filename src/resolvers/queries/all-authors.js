const Author = require('../../models/author')

module.exports = () => {
  return Author.query()
    .select([
      'id',
      'first_name as firstName',
      'last_name as lastName'
    ])
    .eager('books')
}
