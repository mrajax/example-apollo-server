const Book = require('../../models/book')

module.exports = async (parent, args) => {
  const { name, description } = args
  const { id } = await Book.query().insert({ name, description })

  return Book.query()
    .select()
    .eager('authors')
    .modifyEager('authors', builder => {
      builder.select([
        'id',
        'first_name as firstName',
        'last_name as lastName'
      ])
    })
    .findById(id)
}
