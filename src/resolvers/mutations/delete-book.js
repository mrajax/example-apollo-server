const { UserInputError } = require('apollo-server')
const Book = require('../../models/book')

module.exports = async (parent, { id }) => {
  const book = await Book.query().findById(id)
  if (!book) throw new UserInputError('Book not found')
  const deletedBook = await Book.query().deleteById(id).returning('id')
  return deletedBook.id
}
