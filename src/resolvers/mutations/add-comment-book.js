const { UserInputError } = require('apollo-server')
const User = require('../../models/user')
const Book = require('../../models/book')

module.exports = async (parent, args, context) => {
  const { bookId, userId, body } = args
  const { pubsub } = context

  let book, user

  await Book.query()
    .select()
    .findById(bookId)
    .then(b => book = b)
    .catch(() => {})

  if (!book) throw new UserInputError('Book not found')

  userId && await User.query()
    .select()
    .findById(userId)
    .then(u => user = u)
    .catch(() => {})

  if (userId && !user) throw new UserInputError('User not found')

  const { id } = await book
    .$relatedQuery('comments')
    .insert({
      user_id: (user && user.id) || null,
      body
    })

  const addedComment = await book
    .$relatedQuery('comments')
    .select([
      'id',
      'body',
      'user_id as userId',
      'book_id as bookId',
      'created_at as createdAt',
      'updated_at as updatedAt'
    ])
    .findById(id)

  pubsub.publish('COMMENT_ADDED', addedComment)

  return addedComment
}
