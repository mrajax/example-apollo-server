const Author = require('../../models/author')

module.exports = async (parent, args) => {
  const { firstName, lastName } = args

  const { id } = await Author.query().insert({
    first_name: firstName,
    last_name: lastName
  })

  return Author.query()
    .select([
      'id',
      'first_name as firstName',
      'last_name as lastName'
    ])
    .eager('books')
    .findById(id)
}
