const { UserInputError } = require('apollo-server')
const Book = require('../../models/book')

module.exports = async (parent, args) => {
  const { bookId, authorIds } = args

  let book

  await Book.query()
    .eager('authors')
    .modifyEager('authors', builder => {
      builder.select([
        'id',
        'first_name as firstName',
        'last_name as lastName'
      ])
    })
    .findById(bookId)
    .then(b => book = b)
    .catch(() => {})

  if (!book) throw new UserInputError('Book not found')

  try {
    await book
      .$relatedQuery('authors')
      .relate(authorIds)
  } catch (e) {
    const relatedAuthorIds = await book.$relatedQuery('authors').pluck('id')
    const invalidAuthorIds = authorIds.filter(authorId => relatedAuthorIds.includes(authorId))
    throw new UserInputError('Authors are already linked or do not exist', { invalidAuthorIds })
  }

  return book
}
