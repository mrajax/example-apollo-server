const path = require('path')
const config = require('./src/config')

const {
  database: {
    host,
    user,
    password,
    database
  }
} = config

const directoryDatabase = path.join(__dirname, 'src', 'database')
const directoryMigrations = path.join(directoryDatabase, 'migrations')
const directorySeeds = path.join(directoryDatabase, 'seeds')
const migrationStubPath = path.join(directoryDatabase, 'stubs', 'migration.stub')
const seedStubPath = path.join(directoryDatabase, 'stubs', 'seed.stub')

module.exports = {
  client: 'pg',
  version: '11.5',
  connection: {
    host,
    user,
    database,
    password
  },
  migrations: {
    directory: directoryMigrations,
    tableName: 'migrations',
    stub: migrationStubPath
  },
  seeds: {
    directory: directorySeeds,
    stub: seedStubPath
  }
}
